def clean() {
    sh 'mvn clean'
}
def compile() {
    sh 'mvn compile'
}
def stability() {
    sh 'mvn pmd:pmd'
}
def quality() {
    sh 'mvn checkstyle:checkstyle'
}
def coverage() {
    sh 'mvn cobertura:cobertura'
}
